import { Component, h, Prop } from "@stencil/core";

@Component({
    tag: 'webtraining-personal-card',
    styleUrl: 'webtraining-personal-card.css',
    shadow: true
})


export class WebTrainingPersonalCard {


    @Prop() firstName: string;
    @Prop() lastName: string;
    @Prop() resume: string;
    @Prop() photo: string;

    render() {
        return(
            <div class="contenedor-card">
                <div class="card">
                    <div class="contenedor-image">
                        <img src={this.photo} alt="foto" />
                    </div>

                    <h2 class="titulo">{this.firstName} {this.lastName}</h2>

                    <div class="informacion">{this.resume}</div>
                </div>
            </div>
        )
    }
}